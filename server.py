#!/usr/bin/python2
#GPL3
#By: David Hamner
#inventory.py
#Copyright (C) 2019
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.


#screen stuff
# Copyright (c) 2017 Adafruit Industries
# Author: Tony DiCola & James DeVito
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import subprocess
import time
import RPi.GPIO as GPIO
import time
import threading

import time

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import subprocess

greenPin= 17
bluePin = 22
on_off = 27


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(15,GPIO.OUT)

for pin in [greenPin, bluePin]:
	GPIO.setup(pin, GPIO.OUT)
GPIO.setup(on_off, GPIO.IN, pull_up_down=GPIO.PUD_UP)

IPMI_should_be_false = ['Power Overload', 'Main Power Fault', 'Power Control Fault', 'Drive Fault', 'Cooling/Fan Fault']
HOT = 31 #30 #30 Celsius (86 Fahrenheit)
OVERHEAT_REBOOT = HOT - 4
OVERHEATED = False


#Screen config
# Raspberry Pi pin configuration:
RST = None     # on the PiOLED this pin isnt used
# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0


# Load default font.
font = ImageFont.load_default()

# Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
# font = ImageFont.truetype('Minecraftia.ttf', 8)


def log(strr):
	print(strr)
	with open("/server.log", 'a+') as fh:
		fh.write(str(strr) + '\n')
#Thanks https://stackoverflow.com/a/136280
def tail(f, n=12):
    proc = subprocess.Popen(['tail', '-n', str(n), f], stdout=subprocess.PIPE)
    lines = proc.stdout.readlines()
    return lines

IPMI_ = "/home/pi/.ipmi.txt"

def read_ipmi_file(path):
    returnData = {}
    with open(path) as fh:
        for line in fh.readlines():
            if line != "" and not line.startswith("#"):
                rawData = line.split(':')
                returnData['ip'] = rawData[0]
                returnData['user'] = rawData[1]
                returnData['password'] = rawData[2].strip()
    return returnData

            
def IPMI_cmd_str(server):
    return "ipmitool -I lanplus -H " + server['ip'] + " -U " + server['user'] + " -P " + server['password']+ " "


def IPMI_health_check(server):
    cmd = IPMI_cmd_str(server) + "chassis status"
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    errors_found = []
    power = False
    if p.returncode == 0:
        out = out.decode('UTF-8')
        for line in out.split('\n'):
            for test in IPMI_should_be_false:
                if test in line:
                    if 'false' not in line:
                        errors_found.append(test)
                if 'System Power' in line:
                    if 'on' in line:
                        power = True
        return [errors_found, power]
    else:
        return [err.decode('UTF-8')]

def IPMI_temp(server):
    cmd = IPMI_cmd_str(server) + "sdr elist full"
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    
    if p.returncode != 0:
       log("IPMI connection error! File:\n  " + str(err) + "\n  " + cmd)
       return err.decode('UTF-8')
    else:
       return out.decode('UTF-8').split("Front")[-1].split("degrees")[0].split("|")[-1].strip()

def IPMI_off(server):
    cmd = IPMI_cmd_str(server) + "chassis power soft"
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()

def IPMI_on(server):
    cmd = IPMI_cmd_str(server) + "chassis power on"
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()

def LEDs():
	global LED_STATUS
	error_blink_on = True
	while True:
		time.sleep(.5)
		if "off" in LED_STATUS:
			GPIO.output(15,GPIO.HIGH)
			GPIO.output(18,GPIO.LOW)
		if "on" in LED_STATUS:
			GPIO.output(18,GPIO.HIGH)
			GPIO.output(15,GPIO.LOW)

		if "errors" in LED_STATUS:		
			#blink blue
			if error_blink_on:
				GPIO.output(bluePin,GPIO.HIGH)
				error_blink_on = False
			else:
				GPIO.output(bluePin,GPIO.LOW)
				error_blink_on = True 
				
			GPIO.output(greenPin,GPIO.LOW)
		if "ok" in LED_STATUS:
			GPIO.output(greenPin,GPIO.HIGH)
			GPIO.output(bluePin,GPIO.LOW)

global LED_STATUS
LED_STATUS = []


t =  threading.Thread(target=LEDs)
t.daemon = True
t.start()
server = read_ipmi_file(IPMI_)
while True:
	server_should_be = GPIO.input(on_off)
	cmd = "hostname -I | cut -d\' \' -f1"
	IP = subprocess.check_output(cmd, shell=True)
	log("\nUpdating IPMI...")
	try:
		errors = IPMI_health_check(server)
		power_status = errors[1]
	except Exception:
		log("Error pulling power, sleeping...")
		time.sleep(20)
		continue
	temp = IPMI_temp(server)

	draw.rectangle((0,0,width,height), outline=0, fill=0)
	#draw.text((x, top+8),     str(CPU), font=font, fill=255)
	#draw.text((x, top+16),    str(MemUsage),  font=font, fill=255)
	#draw.text((x, top+25),    str(Disk),  font=font, fill=255)
	offset = 0
	if power_status:
		powerTxt = 'On'
	else:
		powerTxt = 'Off'
	LED_STATUS = []
	if temp.isdigit():
		#server is on and has a temp... lets check for errors:
		log("Temp: " +  temp + "C")
		draw.text((x, top + offset), "Power: " + powerTxt + " " + "Temp: " + temp,  font=font, fill=255)
		offset = offset + 8
		if OVERHEATED:
			if int(temp) < int(OVERHEAT_REBOOT):
				OVERHEATED = False
		errors = errors[0]
		if OVERHEATED:
			errors.append("Room overheated: " + temp)
		#check if we are too hot
		if int(temp) >= HOT:
			OVERHEATED = True
			errors.append("OVER TEMP: " + temp)
			IPMI_off(server)
		if errors == []:
			log("Health: OK")
			LED_STATUS.append("ok")
		else:
			LED_STATUS.append("errors")
			log("Errors!!!")
			log(errors)
		if power_status:
			log("Power status: ON")
			LED_STATUS.append("on")
			if server_should_be != 0:
				log("Power OFF!!!")
				IPMI_off(server)
		else:
			log("Power status: OFF")
			LED_STATUS.append("off")
			if server_should_be != 1:
				if not OVERHEATED:
					log("Power ON!!!")
					IPMI_on(server)
				else:
					log("Cannot start...")
					errors.append("Cannot start...")
	else:
		errors.append("IPMI error: " + temp)
		log("IPMI error: " + temp)
		LED_STATUS.append("errors")
	if errors != []:
		draw.text((x, top + offset),       "Errors: " + IP,  font=font, fill=255)
		offset = offset + 8
	else:
		draw.text((x, top + offset),       "Status: OK",  font=font, fill=255)
		offset = offset + 8
		draw.text((x, top + offset), IP,  font=font, fill=255)
		
	for error in errors:
		draw.text((x, top + offset), error,  font=font, fill=255)
		offset = offset + 8
		
	# Display image.
	disp.image(image)
	disp.display()
	time.sleep(10)
